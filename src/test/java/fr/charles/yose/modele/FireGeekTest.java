package fr.charles.yose.modele;

import com.google.common.collect.Lists;
import fr.charles.yose.modele.fireGeek.FireGeek;
import fr.charles.yose.modele.fireGeek.Mouvement;
import org.junit.Test;

import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

public class FireGeekTest {

    @Test
    public void peutDonnerLesMouvementsSimple() {
        FireGeek fireGeek = new FireGeek(Lists.newArrayList("...", "P..", ".WF"));

        List<Mouvement> mouvements = fireGeek.getMouvements();

        assertThat(mouvements).isNotNull();
        assertThat(mouvements.size()).isEqualTo(3);
        assertThat(mouvements.get(0)).isEqualTo(new Mouvement(1, 0));
        assertThat(mouvements.get(1)).isEqualTo(new Mouvement(0, 1));
        assertThat(mouvements.get(2)).isEqualTo(new Mouvement(0, 1));
    }

    @Test
    public void peutContournerLeFeu() {
        FireGeek fireGeek = new FireGeek(Lists.newArrayList(".P.",".F.",".W."));

        List<Mouvement> mouvements = fireGeek.getMouvements();

        assertThat(mouvements.size()).isEqualTo(5);
        assertThat(mouvements.get(0)).isEqualTo(new Mouvement(0, 1));
        assertThat(mouvements.get(1)).isEqualTo(new Mouvement(1, 0));
        assertThat(mouvements.get(2)).isEqualTo(new Mouvement(1, 0));
        assertThat(mouvements.get(3)).isEqualTo(new Mouvement(0, -1));
    }
}
