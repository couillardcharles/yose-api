package fr.charles.yose.modele;

import fr.charles.yose.modele.facteursPremier.FacteursPremier;
import org.junit.Test;

import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

public class FacteursPremierTest {

    @Test
    public void peutDécomposer1() {
        assertThat(décompose(1)).isEmpty();
    }

    @Test
    public void peutDécomposer2() {
        assertThat(décompose(2)).containsExactly(2);
    }

    @Test
    public void peutDécomposer3() {
        assertThat(décompose(3)).containsExactly(3);
    }

    @Test
    public void peutDécomposer4() {
        assertThat(décompose(4)).containsExactly(2, 2);
    }

    @Test
    public void peutDécomposer9() {
        assertThat(décompose(9)).containsExactly(3, 3);
    }

    private List<Integer> décompose(int valeur) {
        return new FacteursPremier().décompose(valeur);
    }
}
