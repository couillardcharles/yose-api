package fr.charles.yose.commande;

import fr.charles.yose.modele.fireGeek.Mouvement;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

public class FireGeekCommandeTest {

    @Before
    public void avant() {
        commande = new FireGeekCommande();
    }

    @Test
    public void peutObtenirLaMap() {
        Resultat<FireGeekResultat> résultat = commande.exécute(3, "...P...WF");

        assertThat(résultat).isNotNull();
        assertThat(résultat.status).isEqualTo(Resultat.Status.SUCCES);
        List<String> map = résultat.donnée.map;
        assertThat(map).isNotNull();
        assertThat(map.size()).isEqualTo(3);
        assertThat(map.get(0)).isEqualTo("...");
        assertThat(map.get(1)).isEqualTo("P..");
        assertThat(map.get(2)).isEqualTo(".WF");
    }

    @Test
    public void peutObtenirLesMouvements() {
        Resultat<FireGeekResultat> résultat = commande.exécute(3, "...P...WF");

        assertThat(résultat).isNotNull();
        assertThat(résultat.status).isEqualTo(Resultat.Status.SUCCES);
        List<Mouvement> moves = résultat.donnée.moves;
        assertThat(moves).isNotNull();
    }

    private FireGeekCommande commande;
}
