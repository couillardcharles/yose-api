package fr.charles.yose.commande;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.fest.assertions.Assertions.assertThat;

public class FacteursPremierCommandeTest {

    @Before
    public void avant() {
        commande = new FacteursPremierCommande();
    }

    @Test
    public void peutObtenirUnRésultat() {
        Resultat<List<Integer>> résultat = commande.exécute("4");

        assertThat(résultat).isNotNull();
        assertThat(résultat.status).isEqualTo(Resultat.Status.SUCCES);
        assertThat(résultat.donnée).containsExactly(2, 2);
    }

    @Test
    public void peutObtenirUneErreurSiLeNombreEstSupérieurA1000000() {
        Resultat<List<Integer>> résultat = commande.exécute("1000001");

        assertThat(résultat.status).isEqualTo(Resultat.Status.ERREUR);
        assertThat(résultat.messageErreur).isEqualTo("too big number (>1e6)");
    }

    @Test
    public void peutObtenirUneErreurSiLaValeurNEstPasUnNombre() {
        Resultat<List<Integer>> résultat = commande.exécute("hello");

        assertThat(résultat.status).isEqualTo(Resultat.Status.ERREUR);
        assertThat(résultat.messageErreur).isEqualTo("not a number");
    }

    @Test
    public void peutObtenirUneErreurSiLeNombreEstInférieurAUn() {
        Resultat<List<Integer>> résultat = commande.exécute("1");

        assertThat(résultat.status).isEqualTo(Resultat.Status.ERREUR);
        assertThat(résultat.messageErreur).isEqualTo("1 is not an integer > 1");
    }

    private FacteursPremierCommande commande;
}
