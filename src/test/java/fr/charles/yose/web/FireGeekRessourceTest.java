package fr.charles.yose.web;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.restlet.ext.json.JsonRepresentation;

import static org.fest.assertions.Assertions.assertThat;

public class FireGeekRessourceTest {

    @Before
    public void before() {
        ressource = new FireGeekRessource();
        InitialisateurRessource.pour(ressource).avecQuery("width", "3").avecQuery("map", "...P...WF").initialise();
    }

    @Test
    public void peutReprésenterAvecMap() throws JSONException {
        JsonRepresentation representation = (JsonRepresentation) ressource.représente();

        JSONObject jsonObject = representation.getJsonObject();
        assertThat(jsonObject.has("map")).isTrue();
        JSONArray map = jsonObject.getJSONArray("map");
        assertThat(map.length()).isEqualTo(3);
        assertThat(map.getString(0)).isEqualTo("...");
        assertThat(map.getString(1)).isEqualTo("P..");
        assertThat(map.getString(2)).isEqualTo(".WF");
    }

    @Test
    public void peutReprésenterAvecMouvements() throws JSONException {
        JsonRepresentation representation = (JsonRepresentation) ressource.représente();

        JSONObject jsonObject = representation.getJsonObject();
        assertThat(jsonObject.has("moves")).isTrue();
        JSONArray moves = jsonObject.getJSONArray("moves");
        assertThat(moves.length()).isNotEqualTo(0);
    }

    private FireGeekRessource ressource;
}
