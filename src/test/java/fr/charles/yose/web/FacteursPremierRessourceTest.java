package fr.charles.yose.web;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.restlet.ext.json.JsonRepresentation;

import static org.fest.assertions.Assertions.assertThat;

public class FacteursPremierRessourceTest {

    @Before
    public void before() {
        ressource = new FacteursPremierRessource();
    }

    @Test
    public void peutReprésenter() throws JSONException {
        InitialisateurRessource.pour(ressource).avecQuery("number", "16").initialise();

        JsonRepresentation representation = (JsonRepresentation) ressource.représente();

        JSONObject jsonObject = representation.getJsonObject();
        assertThat(jsonObject.getInt("number")).isEqualTo(16);
        JSONArray decomposition = jsonObject.getJSONArray("decomposition");
        assertThat(decomposition.length()).isEqualTo(4);
    }

    @Test
    public void peutReprésenterAvecUnNombreQuiNEstPasUnNombre() throws JSONException {
        InitialisateurRessource.pour(ressource).avecQuery("number", "hello").initialise();

        JsonRepresentation representation = (JsonRepresentation) ressource.représente();

        JSONObject jsonObject = representation.getJsonObject();
        assertThat(jsonObject.getString("number")).isEqualTo("hello");
        assertThat(jsonObject.getString("error")).isEqualTo("not a number");
    }

    @Test
    public void peutReprésenterAvecPlusieursParametres() throws JSONException {
        InitialisateurRessource.pour(ressource).avecQuery("number", "hello").avecQuery("number", "haha").initialise();

        JsonRepresentation representation = (JsonRepresentation) ressource.représente();

        JSONArray jsonArray = representation.getJsonArray();
        assertThat(jsonArray.length()).isEqualTo(2);
        JSONObject premierJsonObject = jsonArray.getJSONObject(0);
        assertThat(premierJsonObject.getString("number")).isEqualTo("hello");
        JSONObject deuxiemeJsonObject = jsonArray.getJSONObject(1);
        assertThat(deuxiemeJsonObject.getString("number")).isEqualTo("haha");
    }

    private FacteursPremierRessource ressource;
}
