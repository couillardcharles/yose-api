package fr.charles.yose;

import com.google.common.base.Optional;
import fr.charles.yose.web.YoseApplication;
import org.restlet.Component;
import org.restlet.data.Protocol;

public class Main {

    public static void main(String[] args) throws Exception {
        Component component = new Component();
        component.getServers().add(Protocol.HTTP, port());
        component.getDefaultHost().attach(new YoseApplication());
        component.start();
    }

    private static Integer port() {
        Optional<String> heroku = Optional.fromNullable(System.getenv("PORT"));
        return Integer.valueOf(heroku.or("8182"));
    }

}
