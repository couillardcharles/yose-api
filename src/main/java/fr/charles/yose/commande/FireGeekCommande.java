package fr.charles.yose.commande;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import fr.charles.yose.modele.fireGeek.FireGeek;

import java.util.List;

public class FireGeekCommande {

    public Resultat<FireGeekResultat> exécute(int longueur, String map) {
        List<String> lines = stringMapToList(longueur, map);
        return Resultat.succes(new FireGeekResultat(lines, new FireGeek(lines).getMouvements()));
    }

    private List<String> stringMapToList(int longueur, String map) {
        return Lists.newArrayList(Splitter.fixedLength(longueur).split(map));
    }

}
