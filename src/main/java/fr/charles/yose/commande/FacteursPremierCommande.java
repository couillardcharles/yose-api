package fr.charles.yose.commande;

import fr.charles.yose.modele.facteursPremier.FacteursPremier;
import fr.charles.yose.modele.facteursPremier.ValidateurEntier;

public class FacteursPremierCommande {

    public Resultat exécute(String valeur) {
        if (!ValidateurEntier.estValide(valeur)) {
            return Resultat.erreur("not a number");
        }
        Integer nombre = Integer.valueOf(valeur);
        if (nombre > 1000000) {
            return Resultat.erreur("too big number (>1e6)");
        } else if (nombre <= 1) {
            return Resultat.erreur(nombre + " is not an integer > 1");
        }
        return Resultat.succes(new FacteursPremier().décompose(Integer.valueOf(valeur)));
    }

}
