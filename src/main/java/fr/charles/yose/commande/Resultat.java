package fr.charles.yose.commande;

public class Resultat<T> {

    public enum Status {
        SUCCES, ERREUR;
    }

    public static <U> Resultat<U> succes(U donnée) {
        return new Resultat<U>(Status.SUCCES, donnée);
    }

    public static <U> Resultat<U> erreur(String messageErreur) {
        Resultat<U> résultat = new Resultat<U>(Status.ERREUR, null);
        résultat.messageErreur = messageErreur;
        return résultat;
    }

    public boolean enErreur() {
        return status == Status.ERREUR;
    }

    private Resultat(Status status, T donnée) {
        this.status = status;
        this.donnée = donnée;
    }

    public final Status status;
    public String messageErreur;
    public final T donnée;
}
