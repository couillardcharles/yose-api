package fr.charles.yose.commande;

import fr.charles.yose.modele.fireGeek.Mouvement;

import java.util.List;

public class FireGeekResultat {

    public FireGeekResultat(List<String> lines, List<Mouvement> mouvements) {
        map = lines;
        moves = mouvements;
    }

    public List<String> map;
    public List<Mouvement> moves;
}
