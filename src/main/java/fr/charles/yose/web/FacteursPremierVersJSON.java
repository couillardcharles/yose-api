package fr.charles.yose.web;

import fr.charles.yose.commande.FacteursPremierCommande;
import fr.charles.yose.commande.Resultat;
import fr.charles.yose.modele.facteursPremier.ValidateurEntier;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class FacteursPremierVersJSON {

    public JSONArray résultat(String[] valeurs) throws JSONException {
        JSONArray résultat = new JSONArray();
        for (String valeur : valeurs) {
            résultat.put(résultat(valeur, new FacteursPremierCommande().exécute(valeur)));
        }
        return résultat;
    }

    public JSONObject résultat(String valeur, Resultat<List<Integer>> resultat) throws JSONException {
        if (resultat.enErreur()) {
            if (ValidateurEntier.estValide(valeur)) {
                return erreur(Integer.valueOf(valeur), resultat.messageErreur);
            }
            return erreur(valeur, resultat.messageErreur);
        }
        return succes(valeur, resultat.donnée);
    }

    private JSONObject succes(String valeur, List<Integer> resultat) throws JSONException {
        JSONObject résultat = new JSONObject();
        résultat.put("number", valeur);
        résultat.put("decomposition", resultat);
        return résultat;
    }

    private JSONObject erreur(String valeur, String message) throws JSONException {
        JSONObject résultat = new JSONObject();
        résultat.put("number", valeur);
        résultat.put("error", message);
        return résultat;
    }

    private JSONObject erreur(int valeur, String message) throws JSONException {
        JSONObject résultat = new JSONObject();
        résultat.put("number", valeur);
        résultat.put("error", message);
        return résultat;
    }
}
