package fr.charles.yose.web;

import org.json.JSONArray;
import org.json.JSONException;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

public class FacteursPremierRessource extends ServerResource {

    @Get
    public Representation représente() throws JSONException {
        String[] valeurs = getQuery().getValuesArray("number");
        JSONArray résultat = new FacteursPremierVersJSON().résultat(valeurs);
        if (résultat.length() == 1) {
            return new JsonRepresentation(résultat.getJSONObject(0));
        }
        return new JsonRepresentation(résultat);
    }

}
