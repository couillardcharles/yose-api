package fr.charles.yose.web;

import fr.charles.yose.commande.FireGeekCommande;
import fr.charles.yose.commande.FireGeekResultat;
import fr.charles.yose.commande.Resultat;
import fr.charles.yose.modele.fireGeek.Mouvement;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import java.util.List;

public class FireGeekRessource extends ServerResource {

    @Get
    public Representation représente() throws JSONException {
        Resultat<FireGeekResultat> résultat = new FireGeekCommande().exécute(Integer.parseInt(getQueryValue("width")), getQueryValue("map"));
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("map", résultat.donnée.map);
        jsonObject.put("moves", transforme(résultat.donnée.moves));
        return new JsonRepresentation(jsonObject);
    }

    private JSONArray transforme(List<Mouvement> moves) throws JSONException {
        JSONArray resultat = new JSONArray();
        for (Mouvement move : moves) {
            JSONObject object = new JSONObject();
            object.put("dx", move.colonne);
            object.put("dy", move.ligne);
            resultat.put(object);
        }
        return resultat;
    }

}
