package fr.charles.yose.web;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

public class YoseApplication extends Application {

    @Override
    public Restlet createInboundRoot() {
        Router routeur = new Router();
        routeur.attach("/primeFactors", FacteursPremierRessource.class);
        routeur.attach("/fire/geek", FireGeekRessource.class);
        return routeur;
    }

}
