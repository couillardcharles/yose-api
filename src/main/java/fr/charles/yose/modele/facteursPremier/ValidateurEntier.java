package fr.charles.yose.modele.facteursPremier;

public class ValidateurEntier {

    private ValidateurEntier() {

    }

    public static boolean estValide(String valeur) {
        try {
            Integer.parseInt(valeur);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
