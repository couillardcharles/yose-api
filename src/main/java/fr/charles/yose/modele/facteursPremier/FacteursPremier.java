package fr.charles.yose.modele.facteursPremier;

import com.google.common.collect.Lists;

import java.util.List;

public class FacteursPremier {

    public List<Integer> décompose(int valeur) {
        List<Integer> résultat = Lists.newArrayList();
        for (int facteurCourant = 2; valeur > 1; facteurCourant++) {
            while (valeur % facteurCourant == 0) {
                résultat.add(facteurCourant);
                valeur /= facteurCourant;
            }
        }
        return résultat;
    }
}
