package fr.charles.yose.modele.fireGeek;

import com.google.common.base.Objects;

public class Position {
    public Position(int ligne, int colonne) {
        this.ligne = ligne;
        this.colonne = colonne;
    }

    public Position auSud() {
        return new Position(ligne + 1, colonne);
    }

    public Position aLEst() {
        return new Position(ligne, colonne + 1);
    }

    public Position auNord() {
        return new Position(ligne - 1, colonne);
    }

    public Position aLOuest() {
        return new Position(ligne, colonne - 1);
    }

    public Position au(Mouvement mouvement) {
        return new Position(ligne + mouvement.ligne, colonne + mouvement.colonne);
    }

    @Override
    public boolean equals(Object o) {
        Position other = (Position) o;
        return Objects.equal(ligne, other.ligne) && Objects.equal(colonne, other.colonne);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(ligne, colonne);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this).addValue(ligne).addValue(colonne).toString();
    }

    public int ligne;
    public int colonne;
}
