package fr.charles.yose.modele.fireGeek;

import com.google.common.collect.Lists;

import java.util.List;

public class FireGeek {

    private static final char AVION = 'P';
    private static final char EAU = 'W';
    private static final char FEU = 'F';
    private static final char PASSE = 'X';

    public FireGeek(List<String> map) {
        this.map = Lists.newArrayList(map);
        this.lignes = map.size();
        this.colonnes = map.get(0).length();
    }

    public List<Mouvement> getMouvements() {
        List<Mouvement> resultat = Lists.newArrayList();
        resultat.addAll(résoud(positionAvion(), EAU, FEU));
        resultat.addAll(résoud(positionEau(), FEU, 'Q'));
        return resultat;
    }

    private Position positionAvion() {
        return position(AVION);
    }

    private Position positionEau() {
        return position(EAU);
    }

    private Position position(char pattern) {
        int x = 0;
        for (String line : map) {
            int index = line.indexOf(pattern);
            if (index != -1) {
                return new Position(x, index);
            }
            x++;
        }
        return null;
    }

    public List<Mouvement> résoud(Position positionCourante, char cible, char interdit) {
        List<Mouvement> résultat = Lists.newArrayList();
        Mouvement dernierMouvement = Mouvement.AUCUN;
        while (!(cible == contenu(positionCourante))) {
            dernierMouvement = prochainMouvement(positionCourante, dernierMouvement, interdit);
            positionCourante = positionCourante.au(dernierMouvement);
            estPassé(positionCourante);
            résultat.add(dernierMouvement);
        }
        return résultat;
    }

    private Mouvement prochainMouvement(Position positionCourante, Mouvement dernierMouvement, char interdit) {
        for (Mouvement mouvement : Lists.newArrayList(Mouvement.SUD, Mouvement.EST, Mouvement.NORD, Mouvement.OUEST)) {
            if (!dernierMouvement.equals(mouvement.inverse()) && peutAllerA(positionCourante.au(mouvement), interdit)) {
                return mouvement;
            }
        }
        return null;
    }

    private boolean peutAllerA(final Position nouvellePosition, char interdit) {
        return estDansLaMap(nouvellePosition) && !(interdit == contenu(nouvellePosition)) && !(PASSE == contenu(nouvellePosition));
    }

    private boolean estDansLaMap(final Position nouvellePosition) {
        return nouvellePosition.ligne >= 0
                && nouvellePosition.ligne < lignes
                && nouvellePosition.colonne >= 0
                && nouvellePosition.colonne < colonnes;
    }

    private void estPassé(Position position) {
        if (contenu(position) == '.') {
            StringBuilder nouvelleLigne = new StringBuilder(map.get(position.ligne));
            nouvelleLigne.setCharAt(position.colonne, PASSE);
            map.set(position.ligne, nouvelleLigne.toString());
        }
    }

    private char contenu(Position position) {
        return map.get(position.ligne).charAt(position.colonne);
    }

    private final int lignes;
    private final int colonnes;
    private List<String> map;
}
