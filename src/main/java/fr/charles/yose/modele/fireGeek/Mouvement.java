package fr.charles.yose.modele.fireGeek;

import com.google.common.base.Objects;

public class Mouvement {

    public Mouvement(int ligne, int colonne) {
        this.ligne = ligne;
        this.colonne = colonne;
    }

    public Mouvement inverse() {
        return new Mouvement(-ligne, -colonne);
    }

    @Override
    public boolean equals(Object o) {
        Mouvement other = (Mouvement) o;
        return Objects.equal(ligne, other.ligne) && Objects.equal(colonne, other.colonne);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(ligne, colonne);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this).addValue(ligne).addValue(colonne).toString();
    }

    public int ligne;
    public int colonne;
    public static Mouvement SUD = new Mouvement(1, 0);
    public static Mouvement NORD = new Mouvement(-1, 0);
    public static Mouvement OUEST = new Mouvement(0, -1);
    public static Mouvement EST = new Mouvement(0, 1);
    public static Mouvement AUCUN = new Mouvement(0, 0);
}
